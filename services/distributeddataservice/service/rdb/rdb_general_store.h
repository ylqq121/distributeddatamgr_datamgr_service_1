/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef OHOS_DISTRIBUTED_DATA_DATAMGR_SERVICE_RDB_GENERAL_STORE_H
#define OHOS_DISTRIBUTED_DATA_DATAMGR_SERVICE_RDB_GENERAL_STORE_H
#include <atomic>
#include <functional>
#include "metadata/store_meta_data.h"
#include "rdb_asset_loader.h"
#include "rdb_cloud.h"
#include "rdb_store.h"
#include "relational_store_delegate.h"
#include "relational_store_manager.h"
#include "store/general_store.h"
#include "store/general_value.h"
namespace OHOS::DistributedRdb {
class RdbGeneralStore : public DistributedData::GeneralStore {
public:
    using Cursor = DistributedData::Cursor;
    using GenQuery = DistributedData::GenQuery;
    using VBucket = DistributedData::VBucket;
    using VBuckets = DistributedData::VBuckets;
    using Value = DistributedData::Value;
    using Values = DistributedData::Values;
    using StoreMetaData = DistributedData::StoreMetaData;
    using Database = DistributedData::Database;
    using RdbStore = OHOS::NativeRdb::RdbStore;

    explicit RdbGeneralStore(const StoreMetaData &metaData);
    ~RdbGeneralStore();
    int32_t Bind(const Database &database, BindInfo bindInfo) override;
    bool IsBound() override;
    int32_t Execute(const std::string &table, const std::string &sql) override;
    int32_t BatchInsert(const std::string &table, VBuckets &&values) override;
    int32_t BatchUpdate(const std::string &table, const std::string &sql, VBuckets &&values) override;
    int32_t Delete(const std::string &table, const std::string &sql, Values &&args) override;
    std::shared_ptr<Cursor> Query(const std::string &table, const std::string &sql, Values &&args) override;
    std::shared_ptr<Cursor> Query(const std::string &table, GenQuery &query) override;
    int32_t Sync(const Devices &devices, int32_t mode, GenQuery &query, DetailAsync async, int32_t wait) override;
    int32_t RemoveDeviceData(const std::string &device, int32_t mode) override;
    int32_t Watch(int32_t origin, Watcher &watcher) override;
    int32_t Unwatch(int32_t origin, Watcher &watcher) override;
    int32_t Close() override;

private:
    using RdbDelegate = DistributedDB::RelationalStoreDelegate;
    using RdbManager = DistributedDB::RelationalStoreManager;
    using SyncProcess = DistributedDB::SyncProcess;
    using DBBriefCB = DistributedDB::SyncStatusCallback;
    using DBProcessCB = std::function<void(const std::map<std::string, SyncProcess> &processes)>;
    static constexpr uint32_t ITERATE_TIMES = 10000;
    class ObserverProxy : public DistributedDB::StoreObserver {
    public:
        using DBChangedIF = DistributedDB::StoreChangedData;
        using DBChangedData = DistributedDB::ChangedData;
        using DBOrigin = DistributedDB::Origin;
        void OnChange(const DistributedDB::StoreChangedData &data) override;
        void OnChange(DBOrigin origin, const std::string &originalId, DBChangedData &&data) override;
        bool HasWatcher() const
        {
            return watcher_ != nullptr;
        }
    private:
        friend RdbGeneralStore;
        Watcher *watcher_ = nullptr;
        std::string storeId_;
    };
    DBBriefCB GetDBBriefCB(DetailAsync async);
    DBProcessCB GetDBProcessCB(DetailAsync async);

    ObserverProxy observer_;
    RdbManager manager_;
    RdbDelegate *delegate_ = nullptr;
    std::shared_ptr<RdbStore> store_;
    std::shared_ptr<RdbCloud> rdbCloud_ {};
    std::shared_ptr<RdbAssetLoader> rdbLoader_ {};
    BindInfo bindInfo_;
    std::atomic<bool> isBound_ = false;
};
} // namespace OHOS::DistributedRdb
#endif // OHOS_DISTRIBUTED_DATA_DATAMGR_SERVICE_RDB_GENERAL_STORE_H
